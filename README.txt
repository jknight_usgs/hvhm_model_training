Hualapai Valley Hydrologic Model
Forecast Scenario Package

Created 4/21/2022
Modified 5/19/2022


Instructions for downloading model files
----------------------------------------
- create new folder on local drive, e.g. "hvhm_model"
- right click "hvhm" folder, select "Git Bash Here" from context menu
- enter "git clone https://gitlab.com/jknight_usgs/hvhm_model_training.git"


Instructions for initial setup of forecast scenario package
-----------------------------------------------------------
- navigate into new subfolder "hvhm_model_training" within "hvhm_model" folder
- left click address bar on windows explorer, enter "cmd"
- in the command prompt, enter the following commands sequentially:
    - "1_extract_hvhm_env.bat"
    - "2_activate_hvhm_env.bat"
    - "3_unpack_hvhm_env.bat"


Instructions for providing input required to build a new scenario:
-----------------------------------------------------------------
- building and executing a new forecast scenario requires the user
to provide a comma separated values (.csv) file containing locations and
rates of pumping starting in year 2019. This file must be placed in the
"well_file_inputs" subfolder
	- EX: hvhm_model_training\well_file_inputs\forecast_20220422.csv

- forecast well input file must be formatted with one well location per row,
and contain at least these columns:
	- map_id (integer identifier for use in map plots)
	- UTM_X_METER (easting in NAD83 UTM 12N [epsg: 26912])
	- UTM_Y_METER (northing in NAD83 UTM 12N [epsg: 26912])
	- 2019 (volumetric flux in year 2019, in acre feet per year)
	- add columns for additional years (e.g. 2020, 2021, 2050) up to 2219
	
	WELL INPUT NOTE 1:
	-----------------
	Volumetric flux must be negative for extraction, and positive for
	injection. Zero values are valid, representing years of no pumping or
	injecting. A "well" row can be added where you would like to track
	simulated gw levels without simulating pumping by entering zero for
	each year column included in the file.

	WELL INPUT NOTE 2:
	-----------------
	The forecast built from this file assumes that well fluxes specified for
	a given year continue until a subsequent year when different fluxes are
	specified. To stop pumping in a future year, enter a values of zero in
	that year. For instance, if pumping rates are specified for wells in
	2019 and 2029, and values of zero are entered for 2039, rates specified
	for 2019 will continue until 2029, at which point the 2029 rates become
	active and continue until 2039, when all pumping will cease.

	WELL INPUT NOTE 3:
	-----------------
	Pumping can only be simulated in model cells so long as those cells do not
	become completely dewatered during the simulation. When selecting scenario
	pumping locations, use the included shapefile "hvhm_model_grid.shp" located
	in the "hvhm_grid" subfolder to identify portions of the basin with
	sufficient simulated saturated thickness that can withstand extended periods
	of simulated pumping. The shapefile attribute "sat_2018" contains simulated
	saturated thickness IN METERS at the end of 2018, calculated as simulated
	groundwater elevation minus cell bottom elevation. The attribute values are
	clipped to model cells simulating basin-fill alluvium that are not simulated
	to be dry at the end of 2018.


Instructions for building and executing a new forecast scenario
---------------------------------------------------------------
- unless continuing immediately from executing the initial setup steps in the
  same open command prompt, python environment "hvhm" must be activated.
- To activate the python environment:
	- left click address bar on windows explorer, enter "cmd"
	- in the command prompt, enter "2_activate_hvhm_env.bat"

- When activated, the command prompt will be prepended with the name of the
  environment, e.g.:
	(hvhm) C:\hvhm_model\hvhm_model_training>

- to build and execute a new forecast scenario, in the command prompt enter:
	python update_model_forecast.py

- the script asks for a series of inputs needed to build the model.
	
	- When asked for the name of the file containing forecasted pumping rates,
	  type the name of the user-produced csv file located in the
	  well_file_inputs subfolder, e.g. forecast_20220422.csv
	
	- when asked if the user-produced csv file includes new pumping rates for
	  Kingman municipal wells, enter y or n

	  NOTE:
	  ----
	  If you respond "n" the forecast will simulate Kingman municipal pumping
	  rates of approximately 9,000 acre-feet per year through the end of the
	  simulation.
	
	- when asked to provide a list of years for which to produce gw level
	  contours, enter a list of years separated by commas, e.g. 2022, 2032, 2100
	
	- when asked for number of realizations, enter a number between 2 - 40

	- when asked if you want to compare results to a previously completed
	  scenario, type the name of the subfolder that contains the results to that
	  scenario, e.g. forecast_20220421_master

	    NOTE:
	    ----
	    To compare scenario results to those of the forecast scenario published
	    in USGS report SIR2021-5077, enter "hvhm_published_master" 

	- if the current scenario was previously run, the script will ask if you
	  want to overwrite the results in the existing subfolder. Enter y or n
		
		NOTE:
		----
		If you want do not want to lose the results from that previous scenario
		which used the same csv input file, make a copy of the folder before
		running the script

- The script leverages a parallel run manager to run the specified number of
  model realizations. It is common to receive a Windows firewall warning when
  the run manager initializes. This warning can be ignored (click "cancel").
  The run manager uses an open communications port to communicate to itself the
  progress of the model realizations, but there is no data being sent or
  received externally from the machine.


Forecast scenario outputs
-------------------------
- Outputs are saved to the "model_plots" folder, within a subfolder reflecting
  the name of the well input file. If the same scenario is rerun with the same
  well input file name, the outputs in the scenario subfolder will be
  overwritten unless copied to a different location before running the script.

- Each scenario subdirectory contains a further subdirectory titled
  "shapefiles" containing polyline ESRI .shp files representing simulated
  groundwater levels, depth to water, and saturated thickness IN FEET for at
  least the year 2022 and optionally any years provided to the scenario script
  when prompted. If years beyond 2022 are provided at the prompt, the shapefile
  subdirectory will also contain simulated groundwater level drawdowns from 2022
  to each year provided. If the years 2032, 2042 are provided at the prompt, the
  shapefile subdirectory will contain shapefiles of simulated groundwater level
  drawdowns 2022 - 2032 and 2022 - 2042.

- Outputs are saved as pdfs prepended with the forecast scenario name, derived
  from the well input file provided to the script by the user.

<forecast_name>_sim_rch.pdf: Time series plot showing total specified and
    simulated volumetric flux of recharge by year. The red line is specified
    flux, the gray lines are simulated fluxes for individual model realizations.

<forecast_name>_sim_wel_out.pdf: Time series plot showing total specified and
    simulated volumetric flux of wells by year. The red line is specified flux,
    the gray lines are simulated fluxes for individual model realizations.

<forecast_name>_KingmanAvgDtw_YrReach1200ft.pdf: A histogram showing the
    frequency of model realizations simulating an average depth to water in the
    Kingman subbasin greater than 1200 feet in a given year. If the chart is
    empty, it probably indicates that the average DTW in the Kingman subbasin
    never reaches 1200 ft in any of the model realizations.

<forecast_name>_KingmanAvgDtw_ts.pdf: Time series plot showing the average depth
    to water simulated in the Kingman subbasin (alluvial basin fill sediment
    south of Long Mountain)

<forecast_name>_GwLevels_NewLocs.pdf: Time series plots of simulated
    groundwater levels and depth to groundwater at the locations of wells
    included in the user-provided well input file

<forecast_name>_GwLevels_NewLocs_map.pdf: Map of locations where simulated
    groundwater levels are being tracked due to the presence of a well included
    in the user-provided well input file.

<forecast_name>_GwLevels_ReportLocs.pdf: Time series plots of simulated
    groundwater levels and depth to groundwater at the locations highlighed in
    the published model report.

<forecast_name>_GwLevels_ReportLocs_map.pdf: Map of groundwater level
    observation locations highlighed in the published model report.

<forecast_name>_SpecSimWellFlux_ts.pdf: Time series plots for each well location
    included in the user-provided well input csv file showing the specified
    rates of pumping (provided by the user in the csv file) and the simulated
    rates (rates able to be simulated by the model). Typically, lower simulated
    pumping occurs because the model cell containing the well was simulated to
    go dry.

<forecast_name>_well_and_obs_locs.pdf: Map showing the location of all wells
    (pumping wells, injection wells, or monitoring wells) included in the user-
    provided well input csv file

<forecast_name>_DryDeepMap_byOnset.pdf: Each frame shows the earliest year when
    a given location is simulated by more than 0%, 50%, or 80% realizations to
    go dry or surpass 1200 ft depth to water.

<forecast_name>_DryDeepMap_byYear.pdf: For every 5th simulation year, map shows
    the	percentage of realizations that go dry or surpass 1200 ft depth to water
    Values of 0.0 - 0.2 (yellow dots) in the map for year 2050 indicate that up
    to 20% of the realizations in the ensemble simulated a dry cell or a dtw
    greater than 1200 ft in that location.

<forecast_name>_SimWellByZone_ts.pdf: A time series plot showing simulated well
    fluxes into (infiltration, blue lines) and out (pumping, red lines) of the
    aquifer split by subbasin.