@Echo off
echo extracting (flopy, pyemu, spnspecs) python packages
7z.exe x static_packages.zip -o./static_packages -y > nul

echo extracting processed_data folder
7z.exe x processed_data.zip -y > nul

echo extracting model files folder
7z.exe x hvhm_published_master.zip -y > nul

echo extracting hvhm python environment
7z.exe x hvhm.zip -o./hvhm -y > nul