import os
import shutil


def main():
	env_name = "hvhm"
	with open("qt.conf", "w") as ofp:
		ofp.write("[Paths]\n")
		ofp.write("Prefix = {0}\n".format(os.path.join(os.getcwd(), env_name, "Library").replace("\\","/")))
		ofp.write("Binaries = {0}\n".format(os.path.join(os.getcwd(), env_name, "Library", "bin").replace("\\","/")))
		ofp.write("Libraries = {0}\n".format(os.path.join(os.getcwd(), env_name, "Library", "lib").replace("\\","/")))
		ofp.write("Headers = {0}\n".format(os.path.join(os.getcwd(), env_name, "Library", "include", "qt").replace("\\","/")))
		ofp.write("TargetSpec = win32-msvc\n")
		ofp.write("HostSpec = win32-msvc\n")
	shutil.copy2("qt.conf", os.path.join(env_name, "qt.conf"));
	shutil.copy2("qt.conf", os.path.join(env_name, "Library", "bin", "qt.conf"));
	os.remove("qt.conf")


if __name__ == "__main__":	
	main()