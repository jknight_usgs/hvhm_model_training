import sys
sys.path.append('./static_packages')
import os
import time
import shutil
import numpy as np
import pandas as pd
import flopy
import pyemu
from shapely.geometry import Point
import geopandas as gp
import multiprocessing
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.dates as mdates
import matplotlib.gridspec as gridspec
from matplotlib.ticker import StrMethodFormatter
import matplotlib.patheffects as ptheff
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)
import warnings
warnings.filterwarnings("ignore")


def plot_args(cmap="seismic_r", n_bin=10, vmin=-100, vmax=100):
    cmap = matplotlib.cm.get_cmap(cmap)
    colors = [cmap(0.0), cmap(0.5), cmap(1.0)]
    cm = matplotlib.colors.LinearSegmentedColormap.from_list("my_cmap", colors, N=n_bin)
    n_bins_ranges = np.linspace(vmin,vmax,n_bin+1).tolist()
    norm = matplotlib.colors.BoundaryNorm(n_bins_ranges, len(n_bins_ranges))
    return (cm, norm, n_bins_ranges)


def run_forecast(well_file="well_file.csv", num_reals=5, forecast_name="model_test"):
    md = forecast_name + "_master"
    wd = forecast_name + "_worker"

    mod_name = "hvhm_tr"
    pst_name = "hvhm_forecast.pst"
    pst = pyemu.Pst(os.path.join(md, pst_name))

    pe = pyemu.ParameterEnsemble.from_binary(pst=pst, filename=os.path.join(md, "forecast_pars.jcb"))

    if num_reals < pe.shape[0]:
        pe.iloc[:num_reals, :].to_binary(os.path.join(md, "forecast_pars.jcb"))
    else:
        num_reals = pe.shape[0]
        
    print("executing model ensemble using {0} model realizations".format(num_reals))

    if os.path.exists(wd):
        shutil.rmtree(wd)

    shutil.copytree(md, wd, ignore=shutil.ignore_patterns("*.jcb"))

    num_cores = int(multiprocessing.cpu_count() / 2)
    num_workers = min((num_cores, max((1, num_reals))))

    pyemu.os_utils.start_workers(wd, "pestpp-ies.exe", pst_name, num_workers=num_workers,
                                 worker_root=".", master_dir=md, reuse_master=True)

    shutil.rmtree(wd)


def build_forecast(well_file="well_file.csv", forecast_name="model_test", map_years=[2022, 2032, 2042], replace_muni_pumping=False):
    md = forecast_name + "_master"
    wd = forecast_name + "_worker"

    m_d = "hvhm_published_master"

    if os.path.isdir(md):
       yn = input("forecast directory {0} already exists, do you want to overwrite existing files? (y/n)".format(md))
       if yn == "y":
        shutil.rmtree(md)

    print("copying model files from {0} to {1}".format(m_d, md))
    shutil.copytree(m_d, md)
    
    modshp = gp.read_file(os.path.join("hvhm_grid", "hvhm_model_grid.shp"))
    spdf = pd.read_csv("history_model_timestepping_2220.csv")
    spdf.loc[:, "model_file"] = spdf.loc[:, "SP"].apply(lambda x: "WEL_{0:04d}.dat".format(x))
    spdf.loc[:, "obs_time"] = pd.to_datetime(spdf.loc[:, "end_date"]) + pd.to_timedelta(1, unit="d")
    spdf.loc[:, "obs_time"] = [x.strftime('%Y%m%d') for x in spdf.loc[:, "obs_time"]]
    
    template_df = pd.read_csv(os.path.join(md, "list_org", "WEL_0016.dat"),
                              delim_whitespace=True, names=["layer", "row", "column", "flux"])
    if replace_muni_pumping:
        muni_locs = [(1,129,23), (1,129,25), (1,132,21), (1,137,26), (1,136,20), (1,137,22), (1,138,19)]
        ix = template_df.loc[template_df.apply(lambda x: (x.layer, x.row, x.column) in muni_locs, axis=1)].index
        template_df.loc[ix, "flux"] = 0.0

    ag_df = pd.read_csv(os.path.join("well_file_inputs", well_file))
    ag_df.loc[:, "geometry"] = ag_df.apply(lambda x: Point(x.UTM_X_METER, x.UTM_Y_METER), axis=1)
    ag_df = gp.GeoDataFrame(ag_df, geometry="geometry", crs="epsg:26912")

    f, ax = plt.subplots(1,1,figsize=(8,10.5), dpi=150)
    modshp.plot(ax=ax, column="ibound_1")
    ag_df.plot(ax=ax, marker="+", c="k", linewidth=0.5)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_title("{0} well and obs locations".format(forecast_name))
    plt.tight_layout()
    plt.savefig(os.path.join(o_d, "{0}_well_and_obs_locs.pdf".format(forecast_name)))
    plt.close()

    ag_df = gp.sjoin(ag_df, modshp.filter(["row", "column", "ibound_1", "geometry"]))
    ag_df.loc[:, "layer"] = 1
    ag_df.loc[:, "obgnme"] = ag_df.apply(lambda x: "new_{0:02d}_{1:03d}_{2:03d}".format(int(x.layer) - 1,
                                                                                        int(x.row) - 1,
                                                                                        int(x.column) - 1), axis=1)

    spdf.to_csv(os.path.join(md, "spdf.csv"))

    for sp in range(21, len(spdf)):
        yr = spdf.loc[sp, "st_year"]
        if "{0}".format(yr) in ag_df.columns:
            ag_df.loc[:, "flux"] = ag_df.loc[:, "{0}".format(yr)] * 1233.48 / 365.25
            temp = ag_df.filter(["layer", "row", "column", "flux"])
            temp = pd.concat([temp, template_df], ignore_index=True)
            temp = temp.groupby(["layer", "row", "column"]).sum()
            temp.reset_index(inplace=True)
            temp.to_csv(os.path.join(md, "list_org", "WEL_{0:04d}.dat".format(sp)), sep=" ", index=False, header=False)
        else:
            temp.to_csv(os.path.join(md, "list_org", "WEL_{0:04d}.dat".format(sp)), sep=" ", index=False, header=False)

    # FOR PUBLISHED MODEL, ONLY TRACK A SAMPLE OF WELL LOCATIONS.
    if forecast_name == "hvhm_report_model":
        ag_df = ag_df.iloc[::20, :].copy()

    mod_name = "hvhm_tr"
    pst_name = "hvhm_forecast.pst"
    pst = pyemu.Pst(os.path.join(md, pst_name))

    obs = pst.observation_data.copy()

    print("adding scenario wel flux observations to pst control file")
    yr_cols = [x for x in ag_df.columns if x in spdf.loc[:, "st_year"].values.astype(str)]
    max_yr = max(int(x) for x in yr_cols)

    ag_df0 = ag_df.filter(["obgnme"] + yr_cols).copy()
    ag_df0.loc[:, "obgnme"] = ag_df0.loc[:, "obgnme"].apply(lambda x: x.replace("new", "wel"))
    ag_df0 = ag_df0.groupby("obgnme").sum().reset_index()
    ag_df0.to_csv(os.path.join(md, "wel_obs_locs.csv"))
    ag_df0.index = ag_df0.pop("obgnme")

    with open(os.path.join(md, "wel_obs.dat"), "w") as ofp:
        with open(os.path.join(md, "wel_obs.dat.ins"), "w") as ifp:
            ofp.write("obsnme obsval\n")
            ifp.write("pif ~\n")
            ifp.write("l1\n")

            for grp in ag_df0.index.values:
                obsval0 = 0.0
                for t in spdf.loc[:, "obs_time"].values:
                    yr = t[:4]
                    if yr in ag_df0.columns:
                        obsval0 = ag_df0.loc[grp, yr]
                    obsnme = "{0}_{1}".format(grp, t)
                    obs.loc[obsnme, "obsnme"] = obsnme
                    obs.loc[obsnme, "obsval"] = obsval0
                    obs.loc[obsnme, "weight"] = 0.
                    obs.loc[obsnme, "obgnme"] = grp

                    ofp.write("{0} {1}\n".format(obsnme, obsval0))
                    ifp.write("l1 w !{0}!\n".format(obsnme))

    pst.output_files.append('wel_obs.dat')
    pst.instruction_files.append('wel_obs.dat.ins')
    
    shutil.copy2(os.path.join("scripts", "process_wel_obs.py"), os.path.join(md, "process_wel_obs.py"))
    pst.model_command.append('python process_wel_obs.py')


    print("adding new gw level observations to pst control file")
    ag_df1 = ag_df.groupby("obgnme").first().reset_index()
    ag_df1.to_csv(os.path.join(md, "new_obs_locs.csv"))

    with open(os.path.join(md, "new_obs.dat"), "w") as ofp:
        with open(os.path.join(md, "new_obs.dat.ins"), "w") as ifp:
            ofp.write("obsnme obsval\n")
            ifp.write("pif ~\n")
            ifp.write("l1\n")

            for i, r in ag_df1.iterrows():
                #obgnme = "new_{0:02d}_{1:03d}_{2:03d}".format(int(r.layer) - 1, int(r.row) - 1, int(r.column) - 1)
                for t in spdf.loc[:, "obs_time"].values:
                    obsnme = "{0}_{1}".format(r.obgnme, t)
                    obs.loc[obsnme, "obsnme"] = obsnme
                    obs.loc[obsnme, "obsval"] = 0.
                    obs.loc[obsnme, "weight"] = 0.
                    obs.loc[obsnme, "obgnme"] = r.obgnme

                    ofp.write("{0} 0.0\n".format(obsnme))
                    ifp.write("l1 w !{0}!\n".format(obsnme))

    pst.output_files.append('new_obs.dat')
    pst.instruction_files.append('new_obs.dat.ins')
    
    shutil.copy2(os.path.join("scripts", "process_new_obs.py"), os.path.join(md, "process_new_obs.py"))
    pst.model_command.append('python process_new_obs.py')


    skip = 4
    print("adding new gw level time series obs at every {0}th cell".format(skip))
    ib = np.loadtxt(os.path.join(md, "ibound_layer_1.ref"))
    bot = np.loadtxt(os.path.join(md, "botm_layer_1.ref"))
    top = np.loadtxt(os.path.join(md, "model_top.ref"))

    arr = np.zeros_like(ib)
    arr[skip: arr.shape[0]: skip, skip: arr.shape[1]: skip] = 1
    arr = np.where(ib < 2, 0, arr)
    nlocs = len(np.where(arr==1)[0])

    locs = [(0,i,j) for i,j in zip(np.where(arr==1)[0], np.where(arr==1)[1])]
    names = ["hts_{0:02d}_{1:03d}_{2:03d}".format(x[0],x[1],x[2]) for x in locs]
    df = pd.DataFrame(data={"obs_site": names})
    df.loc[:, "k"] = 0
    df.loc[:, "i"] = df.loc[:, "obs_site"].apply(lambda x: int(x.split("_")[2]))
    df.loc[:, "j"] = df.loc[:, "obs_site"].apply(lambda x: int(x.split("_")[3]))
    df.loc[:, "top"] = df.apply(lambda x: top[x.i, x.j], axis=1)
    df.loc[:, "bot"] = df.apply(lambda x: bot[x.i, x.j], axis=1)

    df.to_csv(os.path.join(md, "arr_obs_locs.csv"))

    dfs = []
    for t in spdf.loc[:, "obs_time"].values:
        temp = df.copy()
        temp.loc[:, "obsval"] = 0.
        temp.loc[:, "weight"] = 0.
        temp.loc[:, "obgnme"] = "hts_{0}".format(t)
        temp.loc[:, "obsnme"] = temp.loc[:, "obs_site"].apply(lambda x: "{0}_{1}".format(x, t))
        temp = temp.filter(["obsnme", "obsval", "weight", "obgnme"])
        temp.index = temp.loc[:, "obsnme"]
        dfs.append(temp)

    htsobs = pd.concat(dfs)
    obs = pd.concat([obs, htsobs])

    with open(os.path.join(md, "arr_obs.dat"), "w") as ofp:
        with open(os.path.join(md, "arr_obs.dat.ins"), "w") as ifp:
            ofp.write("obsnme obsval\n")
            ifp.write("pif ~\n")
            ifp.write("l1\n")

            for i,r in htsobs.iterrows():
                ofp.write("{0} 0.0\n".format(r.obsnme))
                ifp.write("l1 w !{0}!\n".format(r.obsnme))

    pst.output_files.append('arr_obs.dat')
    pst.instruction_files.append('arr_obs.dat.ins')
    
    shutil.copy2(os.path.join("scripts", "process_arr_obs.py"), os.path.join(md, "process_arr_obs.py"))
    pst.model_command.append('python process_arr_obs.py')


    print("adding zonebudget obs tracking well inflows and outflows by subbasin")
    zon = np.loadtxt(os.path.join(md, "hvhm_tr.zbr"), skiprows=2, dtype=int)
    zon = np.where(zon==3, 2, zon)
    aliases = {1: "kingman", 2: "hualapai"}
    df = flopy.utils.ZoneBudget(os.path.join(md, "hvhm_tr.cbc"), zon, aliases=aliases).get_dataframes()
    df *= (365.25 / 1233.48)
    df.reset_index(inplace=True)
    df.loc[:, "datetime"] = df.loc[:, "totim"].apply(lambda x: pd.to_datetime("1934-12-31") + np.timedelta64(int(x), "D"))
    df.drop("totim", axis=1, inplace=True)
    df = df.loc[df["name"].apply(lambda x: "WELLS" in x)].copy()
    df.rename(columns={"name": "grp"}, inplace=True)
    df.loc[:, "grp"] = df.apply(lambda x: "{0}_{1}".format(x.grp, x.datetime.strftime(format="%Y%m%d")), axis=1)
    df.drop("datetime", axis=1, inplace=True)
    df.index = df.pop("grp")
    df = df.stack().reset_index()
    df.loc[:, "obsnme"] = df.apply(lambda x: "{0}_{1}".format(x.level_1, x.grp), axis=1)
    df.rename(columns={0: "obsval"}, inplace=True)
    df = df.filter(["obsnme", "obsval"])
    df.to_csv("zon_obs.dat", sep=" ", index=False)

    with open(os.path.join(md, "zon_obs.dat.ins"), "w") as ifp:
        ifp.write("pif ~\n")
        ifp.write("l1\n")

        for i,r in df.iterrows():
            ifp.write("l1 w !{0}!\n".format(r.obsnme))

    temp = df.copy()
    temp.loc[:, "obsval"] = 0.
    temp.loc[:, "weight"] = 0.
    temp.loc[:, "obgnme"] = "zbd"
    temp = temp.filter(["obsnme", "obsval", "weight", "obgnme"])
    temp.index = temp.loc[:, "obsnme"]

    obs = pd.concat([obs, temp])

    pst.output_files.append('zon_obs.dat')
    pst.instruction_files.append('zon_obs.dat.ins')
    
    shutil.copy2(os.path.join("scripts", "process_zon_obs.py"), os.path.join(md, "process_zon_obs.py"))
    pst.model_command.append('python process_zon_obs.py')


    if map_years:
        spdf_map = spdf.loc[spdf["end_year"].apply(lambda x: x in map_years)].copy()
        spdf_map.to_csv(os.path.join(md, "spdf_map.csv"))

        ib = np.loadtxt(os.path.join(md, "ibound_layer_1.ref"))
        locs = np.where(ib > 1)

        with open(os.path.join(md, "map_obs.dat"), "w") as ofp:
            with open(os.path.join(md, "map_obs.dat.ins"), "w") as ifp:
                ofp.write("obsnme obsval\n")
                ifp.write("pif ~\n")
                ifp.write("l1\n")

                for ii, r in spdf_map.iterrows():
                    print("adding basin-wide gw level observations for {0} to pst control file".format(r.obs_time))
                    obgnme = "map_{0}".format(r.obs_time)
                    for i,j in zip(locs[0], locs[1]):
                        obsnme = "map_00_{0:03d}_{1:03d}_{2}".format(int(i), int(j), r.obs_time)
                        obs.loc[obsnme, "obsnme"] = obsnme
                        obs.loc[obsnme, "obsval"] = 0.
                        obs.loc[obsnme, "weight"] = 0.
                        obs.loc[obsnme, "obgnme"] = obgnme

                        ofp.write("{0} 0.0\n".format(obsnme))
                        ifp.write("l1 w !{0}!\n".format(obsnme))

        pst.output_files.append('map_obs.dat')
        pst.instruction_files.append('map_obs.dat.ins')

        shutil.copy2(os.path.join("scripts", "process_map_obs.py"), os.path.join(md, "process_map_obs.py"))
        pst.model_command.append('python process_map_obs.py')
    
    pst.observation_data = obs
    pst.write(os.path.join(md, pst_name), version=2)


def plot_zbd_obs(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                    pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    # plot simulted net withdrawals
    pst = pyemu.Pst(os.path.join(m_d, pst_name))

    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    obs = pst.observation_data
    obs = obs.loc[obs['obgnme']=='zbd'].copy()
    obs.loc[:,"datetime"] = pd.to_datetime(obs.loc[:, "obsnme"].apply(lambda x: x.split("_")[-1]))
    obs.loc[:, "grp"] = obs.loc[:, "obsnme"].apply(lambda x: "_".join(x.split("_")[:-1]))

    obs.loc[:, "median_q"] = oe_pt.loc[:, obs.index].median()
    obs.loc[:, "mean_q"] = oe_pt.loc[:, obs.index].mean()
    obs.loc[:, "min_q"] = oe_pt.loc[:, obs.index].min()
    obs.loc[:, "max_q"] = oe_pt.loc[:, obs.index].max()

    with PdfPages(os.path.join(o_d, "{0}_SimWellByZone_ts.pdf".format(forecast_name))) as pdf:
        f, ax = plt.subplots(1,1,figsize=(6,4), dpi=100)
        for i, g in obs.groupby("grp"):
            color = "r"
            if "from" in i.lower():
                color = "b"
            ls = "-"
            if "hualapai" in i:
                ls="--"
            
            ax.fill_between(g.datetime, g.min_q, g.max_q, facecolor="0.5", alpha=0.25)
            ax.plot(g.datetime, g.median_q, c=color, ls=ls, lw=0.5, label=i.lower())
            
            #ax.plot(g.datetime, g.obsval, drawstyle="steps-post", c=color, lw=0.5, label="specified")
            
            ax.legend(loc=2)
            ax.set_title("{0}      subbasin well fluxes".format(forecast_name))
            ax.set_ylabel("well flux, in acre-feet per year")
            ax.set_xlim([pd.to_datetime("19400101"), pd.to_datetime("22200101")])

        pdf.savefig()
        plt.close()


def plot_wel_inputs(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                    pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    # plot simulted net withdrawals
    pst = pyemu.Pst(os.path.join(m_d, pst_name))

    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    obs1 = pst.observation_data
    wf = obs1.loc[obs1['obgnme']=='flx_wells'].copy()
    wf.loc[:,"datetime"] = pd.to_datetime(wf.obsnme.apply(lambda x: x.split("_")[-1]))
    wf['sp'] = [x for x in range(len(wf))]
    
    wfin = pd.read_csv(os.path.join('processed_data','processed_wel_data_base',
                                'model_simulated_pumping_by_group.csv'), index_col=0)
    cols = {x:int(x[-3:]) for x in wfin.columns}
    wfin.rename(columns=cols, inplace=True)
    wfin = wfin.T
    wfin['sum'] = wfin.sum(axis=1) * 0.000810714 * 365.25
    wf = wf.join(wfin.filter(['sum']), how='left',on='sp')
    
    wf0 = oe_pt.loc[:, [x for x in oe_pt.columns if x in wf.index]].T
    wf0 = wf0.join(wf.filter(['datetime']), how='left')
    wf0.index = wf0['datetime']
    wf0.drop('datetime',axis=1,inplace=True)
    wf0 = wf0 * 0.000810714 * 365.25

    spdf = pd.read_csv(os.path.join(m_d, "spdf.csv"), index_col=0)
    spdf.loc[:, "datetime"] = pd.to_datetime(spdf.loc[:, "obs_time"], format="%Y%m%d")

    q = []
    dts = []
    for ii, r in spdf.iterrows():
        dts.append(r.datetime)
        q.append(np.sum(np.loadtxt(os.path.join(m_d, "list_org", "WEL_{0:04d}.dat".format(ii)))[:, -1]) * 0.000810714 * 365.25)
    
    f,ax = plt.subplots(1,1,figsize=(6.5,3))
    wf0.plot(ax=ax, legend=False, drawstyle="steps-pre", color='0.1', alpha=0.2)
    #ax.plot(wf['datetime'],wf['sum'], drawstyle="steps-pre", linewidth=1.0, color='r')
    ax.plot(dts, q, drawstyle="steps-pre", linewidth=1.0, color='r')
    ax.set_xlabel('')
    ax.set_ylabel('acre-feet per year')
    ax.grid(linewidth=0.5)
    ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))
    ax.set_title("net simulated withdrawals 1935-2220")
    plt.savefig(os.path.join(o_d, "{0}_sim_wel_out.pdf".format(forecast_name)))


def plot_rch_inputs(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                    pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):    
    # plot simulated natural recharge
    pst = pyemu.Pst(os.path.join(m_d, pst_name))

    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    
    obs1 = pst.observation_data
    rf = obs1.loc[obs1['obgnme']=='flx_recharge'].copy()
    rf.loc[:,"datetime"] = pd.to_datetime(rf.obsnme.apply(lambda x: x.split("_")[-1]))
    rf['sp'] = [x for x in range(len(rf))]
    rf0 = oe_pt.loc[:, [x for x in oe_pt.columns if x in rf.index]].T
    rf0 = rf0.join(rf.filter(['datetime']), how='left')
    rf0.index = rf0['datetime']
    rf0.drop('datetime',axis=1,inplace=True)
    rf0 = rf0 * 0.000810714 * 365.25
    
    f,ax = plt.subplots(1,1,figsize=(8,4))
    rf0.plot(ax=ax, legend=False, drawstyle="steps-pre", color='0.1', alpha=0.2)
    ax.plot(rf['datetime'],rf['obsval'] * 0.000810714 * 365.25, drawstyle="steps-pre", linewidth=1.25, color='r')
    ax.set_xlabel('')
    ax.set_ylabel('acre-feet per year')
    ax.grid(linewidth=0.5)
    ax.yaxis.set_major_formatter(StrMethodFormatter('{x:,.0f}'))
    ax.set_title("simulated natural recharge 1935-2220")
    plt.savefig(os.path.join(o_d,"{0}_sim_rch_in.pdf".format(forecast_name)))

    
def plot_dtw_hist(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                  pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    # Plot Hist of year when dtw >= 1200 ft in kingman subbasin
    
    pst = pyemu.Pst(os.path.join(m_d, pst_name))

    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])

    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    dtw_df = oe_pt.loc[:, "dtw_dt_thresh"].copy()
    dtw_dt = pd.to_datetime([str(x)[:8] for x in dtw_df.values])
    dtw_df.to_csv(os.path.join(o_d, "dtw_dt_pt.csv"))

    if a_d:
        oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))
        dtw_df_a = oe_pa.loc[:, "dtw_dt_thresh"].copy()
        dtw_dt_a = pd.to_datetime([str(x)[:8] for x in dtw_df_a.values])
        dtw_df_a.to_csv(os.path.join(o_d, "dtw_dt_pa.csv"))

    bins = np.linspace(2020, 2220, 21)
    f, ax = plt.subplots(1,1, figsize=(6,4), dpi=100)
    im = ax.hist(dtw_dt.year, bins=bins, alpha=0.5, facecolor="b", label=forecast_name)
    if a_d:
        im_a = ax.hist(dtw_dt_a.year, bins=bins, alpha=0.5, facecolor="r", label=a_d.replace("_master", ""))
    ax.set_xlim([2020, 2220])
    ax.xaxis.set_major_locator(MultipleLocator(10))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%d'))
    ax.xaxis.set_minor_locator(MultipleLocator(1))

    ax.set_title("Year when simulated avg DTW >= 1200 ft")
    ax.set_ylabel("count")
    ax.legend(loc=2)
    plt.savefig(os.path.join(o_d, "{0}_KingmanAvgDtw_YrReach1200ft.pdf".format(forecast_name)))

def plot_dtw_ts(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
           pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    # plot AVG DTW Kingman Subbasin timeseries ensemble results
    print("plotting timeseries of AVG DTW Kingman Subbasin\n")

    pst = pyemu.Pst(os.path.join(m_d, pst_name))

    obs_group = pst.observation_data.loc[pst.observation_data['obgnme'] == "dtw_ts"].copy()
    obs_group.loc[:,"datetime"] = pd.to_datetime(obs_group.loc[:, "obsnme"].apply(lambda x: x.split("_")[-1]))

    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    mean_pt = oe_pt.loc[:,obs_group.obsnme].mean(axis=0).values

    if a_d:
        oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))
        mean_pa = oe_pa.loc[:,obs_group.obsnme].mean(axis=0).values
    
    #plot timeseries
    years20 = mdates.YearLocator(20)
    years1 = mdates.YearLocator()
    years_fmt = mdates.DateFormatter('%Y')
    datemin = np.datetime64('1935', 'Y')
    datemax = np.datetime64('2220', 'Y')

    pp = PdfPages(os.path.join(o_d,'{0}_KingmanAvgDtw_ts.pdf'.format(forecast_name)))
    fig, ax = plt.subplots(1,1,figsize=(6.5,3))

    ax.yaxis.set_tick_params(labelsize=6,rotation=0)
    ax.xaxis.set_major_locator(years20)
    ax.xaxis.set_major_formatter(years_fmt)
    ax.xaxis.set_minor_locator(years1)
    ax.xaxis.set_tick_params(labelsize=6,rotation=0)
    ax.set_xlim([datemin, datemax])
    ax.set_title("Simulated Average Depth to Water in Kingman Subbasin")
    ax.set_ylim(1500., 500.)
    ax.set_ylabel('Subbasin-Averaged Depth to Water (feet)')
    ax.hlines(1200., datemin, datemax, colors='k', linestyles='dashed', linewidths = 1.)

    #[ax.plot(obs_group.datetime,oe_pr.loc[r, obs_group.obsnme], color="0.5", lw=0.1, alpha=0.25) for r in oe_pr.index]
    [ax.plot(obs_group.datetime, oe_pt.loc[r, obs_group.obsnme], color="b", lw=0.2, alpha=0.5) for r in oe_pt.index]
    ax.plot(obs_group.datetime, mean_pt, color="b", linestyle="--", lw=1,
            path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()], label=forecast_name)

    if a_d:
        [ax.plot(obs_group.datetime,oe_pa.loc[r, obs_group.obsnme], color="r", lw=0.2, alpha=0.5) for r in oe_pa.index]
        ax.plot(obs_group.datetime, mean_pa, color="r", linestyle="--", lw=1,
                path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()], label=a_d.replace("_master", ""))
    
    # inset = inset_axes(ax, height=2., width=1.5, loc=3)
    # modshp.plot(ax=inset,cmap='Greys',alpha=0.25,rasterized=True)
    # inset.set_xticks([])
    # inset.set_yticks([])
    # inset.patch.set_alpha(0.)
    # inset.axis('off')

    ax.legend(loc=1)

    pp.savefig(fig,dpi=150)
    plt.close(fig)
    pp.close()


def plot_hds_ts(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    print("plotting simulated gw level time series at report locations\n")
    # load model object
    m = flopy.modflow.Modflow.load("hvhm_tr.nam",model_ws=m_d,check=False,forgive=False, load_only=["DIS", "BAS6"])    
    mg = m.modelgrid

    # import model grid for inset location plot
    model_shp = gp.read_file(os.path.join('hvhm_grid','hvhm_model_grid.shp'))
    modshp = model_shp.filter(['ibound_1','geometry']).dissolve(by='ibound_1')
    modshp.drop(0, inplace=True)
    modshp.reset_index(inplace=True)

    # import pst object
    pst = pyemu.Pst(os.path.join(m_d, pst_name))
    
    # import observation ensembles
    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))

    if a_d:
        oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))

    # plot timeseries ensemble results
    plot_locs = pd.read_csv(os.path.join('processed_data','processed_obs_data','plot_list.csv'), index_col=0)
    obs = pst.observation_data.loc[pst.observation_data['obgnme'].isin(plot_locs.index.values)].copy()
    obs['map_id'] = [plot_locs.loc[x,"map_id"] for x in obs['obgnme']]
    obs['map_id'] = obs['map_id'].astype(int)
    obs.loc[:,"category"] = obs.obsnme.apply(lambda x: x.split("_")[0])
    obs.loc[:,"k"] = obs.obsnme.apply(lambda x: int(x.split("_")[1]))
    obs.loc[:,"i"] = obs.obsnme.apply(lambda x: int(x.split("_")[2]))
    obs.loc[:,"j"] = obs.obsnme.apply(lambda x: int(x.split("_")[3]))
    obs.loc[:,"gridloc"] = ["{0:02d}_{1:03d}_{2:03d}".format(k,i,j) for k,i,j in zip(obs['k'],obs['i'],obs['j'])]
    obs.loc[:,"x"] = [mg.xcellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,"y"] = [mg.ycellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]

    obs.loc[:,'top'] = [m.dis.top.array[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,'botm'] = [m.dis.botm.array[k,i,j] for k,i,j in zip(obs.loc[:,"k"], obs.loc[:,"i"], obs.loc[:,"j"])]

    obs.loc[:,"datetime"] = pd.to_datetime(obs.obsnme.apply(lambda x: x.split("_")[4]))
    
    mm = obs.drop_duplicates(subset=['map_id'],keep='first')
    
    # plot map of timeseries locations
    fig, ax = plt.subplots(1,1,figsize=(4,6))
    ax.set_xticks([])
    ax.set_yticks([])
    modshp.plot(ax=ax,cmap='Greys',alpha=0.25,rasterized=True)
    [ax.annotate("{0}".format(m),(x,y), fontsize='small') for m,x,y in zip(mm['map_id'], mm['x'], mm['y'])]
    plt.savefig(os.path.join(o_d, "{0}_GwLevels_ReportLocs_map.pdf".format(forecast_name)))
    
    #plot timeseries
    years20 = mdates.YearLocator(20)
    years1 = mdates.YearLocator()
    years_fmt = mdates.DateFormatter('%Y')
    datemin = np.datetime64('1935', 'Y')
    datemax = np.datetime64('2220', 'Y')

    pp = PdfPages(os.path.join(o_d, "{0}_GwLevels_ReportLocs.pdf".format(forecast_name)))

    ct=0
    for nz_group,site_id,map_id in zip(plot_locs.index.values,plot_locs['site_no'].values,plot_locs['map_id'].values):    
        obs_group = obs.loc[obs.obgnme==nz_group,:]
        nz_obs_group = obs.loc[(obs.obgnme == nz_group) & (obs.weight > 0.),:]
        
        fig, ax = plt.subplots(1,1,figsize=(6.5,3))

        ax.yaxis.set_tick_params(labelsize=6,rotation=0)
        ax.xaxis.set_major_locator(years20)
        ax.xaxis.set_major_formatter(years_fmt)
        ax.xaxis.set_minor_locator(years1)
        ax.xaxis.set_tick_params(labelsize=6,rotation=0)
        ax.set_xlim([datemin, datemax])
    
        #[ax.plot(obs_group.datetime,oe_pr.loc[r,obs_group.obsnme] * 3.28084,color="0.5",lw=0.1,alpha=0.25) for r in oe_pr.index]
        [ax.plot(obs_group.datetime,oe_pt.loc[r, obs_group.obsnme] * 3.28084,color="b",lw=0.2,alpha=0.5) for r in oe_pt.index]
        mean_pt = oe_pt.loc[:,obs_group.obsnme].mean(axis=0).values
        ax.plot(obs_group.datetime, mean_pt * 3.28084, color="b", linestyle="--", lw=1,
                path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()], label=forecast_name)
        
        # ax.plot(obs_group.datetime, oe_pt.loc["base", obs_group.obsnme] * 3.28084, color="k", linestyle="--", lw=2,
        #         path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])
        
        if a_d:
            [ax.plot(obs_group.datetime,oe_pa.loc[r, obs_group.obsnme] * 3.28084,color="r",lw=0.2,alpha=0.5) for r in oe_pa.index]
            mean_pa = oe_pa.loc[:,obs_group.obsnme].mean(axis=0).values
            ax.plot(obs_group.datetime, mean_pa * 3.28084, color="r", linestyle="--", lw=1,
                    path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()], label=a_d.replace("_master", ""))
            
            # ax.plot(obs_group.datetime, oe_pa.loc["base", obs_group.obsnme] * 3.28084, color="k", linestyle="--", lw=2,
            #         path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])

        ax.plot(nz_obs_group.datetime,[x * 3.28084 for x in nz_obs_group.obsval],"k+",markersize=4)
        
        # plot DTW on secondary y axis
        top_elev = nz_obs_group.loc[:,'top'][0] * 3.28084

        ax.set_title("{0}      observation site {1}".format(forecast_name, map_id))
        vmin = nz_obs_group.obsval.max() * 3.28084 - 1000.
        vmax = nz_obs_group.obsval.max() * 3.28084 + 50.
        ax.set_ylim(vmin,vmax)
        ax.set_ylabel('elevation (feet)')

        def alt2dtw(x):
            return top_elev - x
        def dtw2alt(x):
            return top_elev - x
        secaxy = ax.secondary_yaxis('right', functions=(alt2dtw, dtw2alt))
        secaxy.set_ylabel('depth to water (feet)')
    
        ax.plot(obs_group.datetime, [nz_obs_group.loc[:,'botm'][0] * 3.28084] * len(obs_group.datetime),
                color="k", linestyle="--", lw=0.5, label="basement elevation")
        
        inset = inset_axes(ax, height=2., width=1.5, loc=3)
        # modshp.plot(ax=inset,cmap='Greys',alpha=0.25,rasterized=True)
        modshp.geometry.boundary.plot(ax=inset,facecolor="None", edgecolor="0.5",linewidth=0.5, alpha=0.5)
        [inset.annotate("{0}".format(m),(x,y), fontsize='xx-small') for m,x,y in zip(mm['map_id'], mm['x'], mm['y'])]
        inset.annotate("{0}".format(nz_obs_group.loc[:,'map_id'][0]),(nz_obs_group.loc[:,'x'][0],nz_obs_group.loc[:,'y'][0]),c='r', fontsize='xx-small')
        inset.set_xticks([])
        inset.set_yticks([])
        inset.patch.set_alpha(0.)
        inset.axis('off')

        ax.legend(loc=1)

        pp.savefig(fig,dpi=150)
        plt.close(fig)
        ct+=1
    pp.close()


def plot_hds_ts_new(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    print("plotting simulated gw level time series at scenario locations\n")
    # load model object
    m = flopy.modflow.Modflow.load("hvhm_tr.nam",model_ws=m_d,check=False,forgive=False, load_only=["DIS", "BAS6"])    
    mg = m.modelgrid

    # import model grid for inset location plot
    model_shp = gp.read_file(os.path.join('hvhm_grid','hvhm_model_grid.shp'))
    modshp = model_shp.filter(['ibound_1','geometry']).dissolve(by='ibound_1')
    modshp.drop(0, inplace=True)
    modshp.reset_index(inplace=True)

    # import pst object
    pst = pyemu.Pst(os.path.join(m_d, pst_name))
    
    # import observation ensembles
    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))

    if a_d:
        oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))

    # plot timeseries ensemble results
    plot_locs = pd.read_csv(os.path.join(m_d, 'new_obs_locs.csv'), index_col=0)

    plot_locs.index = plot_locs.pop("obgnme")

    obs = pst.observation_data.loc[pst.observation_data['obgnme'].isin(plot_locs.index.values)].copy()
    obs['map_id'] = [plot_locs.loc[x,"map_id"] for x in obs['obgnme']]
    obs['map_id'] = obs['map_id'].astype(int)
    obs.loc[:,"category"] = obs.obsnme.apply(lambda x: x.split("_")[0])
    obs.loc[:,"k"] = obs.obsnme.apply(lambda x: int(x.split("_")[1]))
    obs.loc[:,"i"] = obs.obsnme.apply(lambda x: int(x.split("_")[2]))
    obs.loc[:,"j"] = obs.obsnme.apply(lambda x: int(x.split("_")[3]))
    obs.loc[:,"gridloc"] = ["{0:02d}_{1:03d}_{2:03d}".format(k,i,j) for k,i,j in zip(obs['k'],obs['i'],obs['j'])]
    obs.loc[:,"x"] = [mg.xcellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,"y"] = [mg.ycellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]

    obs.loc[:,'top'] = [m.dis.top.array[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,'botm'] = [m.dis.botm.array[k,i,j] for k,i,j in zip(obs.loc[:,"k"], obs.loc[:,"i"], obs.loc[:,"j"])]

    obs.loc[:,"datetime"] = pd.to_datetime(obs.obsnme.apply(lambda x: x.split("_")[4]))
    
    mm = obs.drop_duplicates(subset=['map_id'],keep='first')
    
    # plot map of timeseries locations
    fig, ax = plt.subplots(1,1,figsize=(4,6))
    ax.set_xticks([])
    ax.set_yticks([])
    modshp.plot(ax=ax,cmap='Greys',alpha=0.25,rasterized=True)
    [ax.annotate("{0}".format(m),(x,y), fontsize='small') for m,x,y in zip(mm['map_id'], mm['x'], mm['y'])]
    plt.savefig(os.path.join(o_d, "{0}_GwLevels_NewLocs_map.pdf".format(forecast_name)))
    
    #plot timeseries
    years20 = mdates.YearLocator(20)
    years1 = mdates.YearLocator()
    years_fmt = mdates.DateFormatter('%Y')
    datemin = np.datetime64('1935', 'Y')
    datemax = np.datetime64('2220', 'Y')

    pp = PdfPages(os.path.join(o_d, "{0}_GwLevels_NewLocs.pdf".format(forecast_name)))

    ct=0
    for nz_group,site_id,map_id in zip(plot_locs.index.values,plot_locs['site_no'].values,plot_locs['map_id'].values):    
        obs_group = obs.loc[obs.obgnme==nz_group,:]
        #nz_obs_group = obs.loc[(obs.obgnme == nz_group) & (obs.weight > 0.),:]
        nz_obs_group = obs_group.copy()
        fig, ax = plt.subplots(1,1,figsize=(6.5,3))

        ax.yaxis.set_tick_params(labelsize=6,rotation=0)
        ax.xaxis.set_major_locator(years20)
        ax.xaxis.set_major_formatter(years_fmt)
        ax.xaxis.set_minor_locator(years1)
        ax.xaxis.set_tick_params(labelsize=6,rotation=0)
        ax.set_xlim([datemin, datemax])
    
        #[ax.plot(obs_group.datetime,oe_pr.loc[r,obs_group.obsnme] * 3.28084,color="0.5",lw=0.1,alpha=0.25) for r in oe_pr.index]
        [ax.plot(obs_group.datetime,oe_pt.loc[r, obs_group.obsnme] * 3.28084,color="b",lw=0.2,alpha=0.5) for r in oe_pt.index]
        mean_pt = oe_pt.loc[:,obs_group.obsnme].mean(axis=0).values
        ax.plot(obs_group.datetime, mean_pt * 3.28084, color="b", linestyle="--", lw=1,
                path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])
        
        # ax.plot(obs_group.datetime, oe_pt.loc["base", obs_group.obsnme] * 3.28084, color="k", linestyle="--", lw=2,
        #         path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])
        
        if a_d:
            try:
                [ax.plot(obs_group.datetime,oe_pa.loc[r, obs_group.obsnme] * 3.28084,color="r",lw=0.2,alpha=0.5) for r in oe_pa.index]
                mean_pa = oe_pa.loc[:,obs_group.obsnme].mean(axis=0).values
                ax.plot(obs_group.datetime, mean_pa * 3.28084, color="r", linestyle="--", lw=1,
                        path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])
                
                # ax.plot(obs_group.datetime, oe_pa.loc["base", obs_group.obsnme] * 3.28084, color="k", linestyle="--", lw=2,
                #         path_effects=[ptheff.Stroke(linewidth=2, foreground='w'), ptheff.Normal()])
            except:
                print("cannot find site {0} in comparing scenario".format(nz_group))

        #ax.plot(nz_obs_group.datetime,[x * 3.28084 for x in nz_obs_group.obsval],"k+",markersize=4)
        
        # plot DTW on secondary y axis
        top_elev = nz_obs_group.loc[:,'top'][0] * 3.28084

        #ax.set_title("{0}     {1}     {2}".format(nz_group,site_id,map_id))
        ax.set_title("{0}      observation site {1}".format(forecast_name, map_id))
        vmin = mean_pt[0] * 3.28084 - 1000.
        vmax = mean_pt[0] * 3.28084 + 50.
        ax.set_ylim(vmin,vmax)
        ax.set_ylabel('elevation (feet)')

        def alt2dtw(x):
            return top_elev - x
        def dtw2alt(x):
            return top_elev - x
        secaxy = ax.secondary_yaxis('right', functions=(alt2dtw, dtw2alt))
        secaxy.set_ylabel('depth to water (feet)')
    
        ax.plot(obs_group.datetime, [nz_obs_group.loc[:,'botm'][0] * 3.28084] * len(obs_group.datetime),
                color="k", linestyle="--", lw=0.5, label="basement elevation")
        # ax.hlines([nz_obs_group.loc[:,'top'][0] * 3.28084,nz_obs_group.loc[:,'botm'][0] * 3.28084],
        #           datemin, datemax, colors=['b','k'], linestyles='dashed', linewidths = 1.)
        
        inset = inset_axes(ax, height=2., width=1.5, loc=3)
        # modshp.plot(ax=inset,cmap='Greys',alpha=0.25,rasterized=True)
        modshp.geometry.boundary.plot(ax=inset,facecolor="None", edgecolor="0.5",linewidth=0.5, alpha=0.5)
        [inset.annotate("{0}".format(m),(x,y), fontsize='xx-small') for m,x,y in zip(mm['map_id'], mm['x'], mm['y'])]
        inset.annotate("{0}".format(nz_obs_group.loc[:,'map_id'][0]),(nz_obs_group.loc[:,'x'][0],nz_obs_group.loc[:,'y'][0]),c='r', fontsize='xx-small')
        inset.set_xticks([])
        inset.set_yticks([])
        inset.patch.set_alpha(0.)
        inset.axis('off')

        ax.legend(loc=1)

        pp.savefig(fig,dpi=150)
        plt.close(fig)
        ct+=1
    pp.close()


def plot_wel_ts(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    
    print("plotting specified and simulated scenario pumping rates\n")
    model_shp = gp.read_file(os.path.join('hvhm_grid','hvhm_model_grid.shp'))
    mshp = model_shp.dissolve(by="ibound_1")
    mshp.drop(0, inplace=True)
    mshp.reset_index(inplace=True)

    m = flopy.modflow.Modflow.load("hvhm_tr.nam",model_ws=m_d,check=False,forgive=False, load_only=["DIS"])    
    mg = m.modelgrid
    xmin, xmax, ymin, ymax = mg.extent

    pst = pyemu.Pst(os.path.join(m_d, "hvhm_forecast.pst"))
    obs = pst.observation_data.copy()
    obs = obs.loc[obs["obsnme"].apply(lambda x: x.startswith("wel_"))].copy()
    obs.loc[:,"k"] = obs.obsnme.apply(lambda x: int(x.split("_")[1]))
    obs.loc[:,"i"] = obs.obsnme.apply(lambda x: int(x.split("_")[2]))
    obs.loc[:,"j"] = obs.obsnme.apply(lambda x: int(x.split("_")[3]))
    obs.loc[:,"datetime"] = pd.to_datetime(obs.obsnme.apply(lambda x: int(x.split("_")[4])), format="%Y%m%d")
    obs.loc[:,"x"] = [mg.xcellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,"y"] = [mg.ycellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]

    oe_pt = pyemu.ObservationEnsemble.from_binary(pst, os.path.join(m_d, "hvhm_forecast.0.obs.jcb"))
    obs.loc[:, "median_q"] = oe_pt.loc[:, obs.index].median()
    obs.loc[:, "mean_q"] = oe_pt.loc[:, obs.index].mean()
    obs.loc[:, "min_q"] = oe_pt.loc[:, obs.index].min()
    obs.loc[:, "max_q"] = oe_pt.loc[:, obs.index].max()

    with PdfPages(os.path.join(o_d, "{0}_SpecSimWellFlux_ts.pdf".format(forecast_name))) as pdf:
        obs_info = obs.groupby("obgnme").last()
        
        for i, g in obs.groupby("obgnme"):
            color = "b"
            if np.mean(g.obsval) < 0.0:
                color = "r"
            
            f, ax = plt.subplots(1,1,figsize=(6,4), dpi=100)
            ax.fill_between(g.datetime, g.min_q, g.max_q, facecolor="0.5", alpha=0.5)
            ax.plot(g.datetime, g.median_q, c="0.5", lw=0.5, label="simulated")
            
            ax.plot(g.datetime, g.obsval, drawstyle="steps-post", c=color, lw=0.5, label="specified")
            
            ax.legend(loc=1)
            ax.set_title("{0}      {1} flux".format(forecast_name, i))
            ax.set_ylabel("well flux, in acre-feet per year")
            
            inset = inset_axes(ax, height=1.5, width=1., loc=3)
            mshp.geometry.boundary.plot(ax=inset,facecolor="None", edgecolor="0.5",linewidth=0.5, alpha=0.5)
            inset.patch.set_alpha(0.)
            inset.axis('off')

            inset.scatter(obs_info.loc[obs_info['obsval'] < 0.0, 'x'],
                          obs_info.loc[obs_info['obsval'] < 0.0, 'y'], c='r',s=5, alpha=0.25)
            inset.scatter(obs_info.loc[obs_info['obsval'] > 0.0, 'x'],
                          obs_info.loc[obs_info['obsval'] > 0.0, 'y'], c='b',s=5, alpha=0.25)

            inset.scatter(obs_info.loc[i,'x'],
                          obs_info.loc[i,'y'],
                          c=color,s=10, alpha=0.75)

            inset.set_xticks([])
            inset.set_yticks([])

            pdf.savefig()
            plt.close()


def plot_drydeep(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                   pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test"):
    # load model object
    m = flopy.modflow.Modflow.load("hvhm_tr.nam",model_ws=m_d,check=False,forgive=False, load_only=["DIS", "BAS6"])    
    mg = m.modelgrid

    # load model grid shapefile
    model_shp = gp.read_file(os.path.join('hvhm_grid','hvhm_model_grid.shp'))
    modshp = model_shp.filter(['layer', 'row', 'column', 'botm_1', 'top', 'ibound_1', 'geometry'])
    modshp.loc[:, "i"] = modshp.loc[:, "row"] - 1
    modshp.loc[:, "j"] = modshp.loc[:, "column"] - 1

    mshp = modshp.dissolve(by="ibound_1")
    mshp.drop(0, inplace=True)
    mshp.reset_index(inplace=True)
    
    # import pst object
    pst = pyemu.Pst(os.path.join(m_d, pst_name))
    
    # retrieve head map observations
    obs = pst.observation_data.loc[pst.observation_data['obgnme'].apply(lambda x: x.startswith("hts_"))].copy()
    obs.loc[:,"datetime"] = obs.loc[:, "obsnme"].apply(lambda x: pd.to_datetime(x.split("_")[4]))
    obs.loc[:, "year"] = obs.loc[:, "datetime"].apply(lambda x: x.year)
    obs.loc[:, "obs_site"] = obs.loc[:, "obsnme"].apply(lambda x: "_".join(x.split("_")[:-1]))

    df = pd.read_csv(os.path.join(m_d, "arr_obs_locs.csv"), index_col=0)
    df.index = df.pop("obs_site")
    obs = obs.join(df, how="left", on="obs_site")
    obs.loc[:,"x"] = mg.xcellcenters[obs.loc[:,"i"], obs.loc[:,"j"]]
    obs.loc[:,"y"] = mg.ycellcenters[obs.loc[:,"i"], obs.loc[:,"j"]]

    # import observation ensembles
    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))
    oe_pt = pd.DataFrame.from_records(oe_pt.to_records())
    nreal = float(len(oe_pt))
    obs.loc[:, "pct_dry"] = obs.apply(lambda x: oe_pt.loc[oe_pt[x.obsnme] < x.bot + 10., x.obsnme].count() / nreal, axis=1)
    obs.loc[:, "pct_low"] = obs.apply(lambda x: oe_pt.loc[oe_pt[x.obsnme] < x.top - (1200. * 0.3048), x.obsnme].count() / nreal, axis=1)

    # if a_d:
    #     oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))
    #     oe_pa = pd.DataFrame.from_records(oe_pa.to_records())
    #     nreal = float(len(oe_pa))
    #     obs.loc[:, "pct_dry_alt"] = obs.apply(lambda x: oe_pa.loc[oe_pa[x.obsnme] < x.bot + 1., x.obsnme].count() / nreal, axis=1)
    #     obs.loc[:, "pct_low_alt"] = obs.apply(lambda x: oe_pa.loc[oe_pa[x.obsnme] < x.top - 1200., x.obsnme].count() / nreal, axis=1)

    # single plot showing year when each area first goes dry or surpasses 1200 ft dtw
    vmin=2020
    vmax = 2220
    cm, norm, n_bins_ranges = plot_args(cmap="viridis", n_bin=10, vmin=vmin, vmax=vmax)

    with PdfPages(os.path.join(o_d, "{0}_DryDeepMap_byOnset.pdf".format(forecast_name))) as pdf:
        for flg, desc in zip(["pct_dry", "pct_low"], ["earliest year dry", "earliest year > 1200 ft DTW"]):
            f, axs = plt.subplots(2,2,figsize=(8.5, 11), dpi=100)
            
            for pct, ax in zip([0.0, 0.5, 0.8], axs.flat):
                temp = obs.loc[obs[flg] > pct].sort_values("datetime").groupby("obs_site").first()

                mshp.geometry.boundary.plot(ax=ax, facecolor="None", edgecolor="0.5", linewidth=0.5)
                im = ax.scatter(temp.loc[:, "x"], temp.loc[:, "y"], c=temp.loc[:, "year"], cmap=cm, norm=norm)

                divider = make_axes_locatable(ax)
                ax_cb = divider.new_horizontal(size="5%", pad=0.2, pack_start=False)
                f.add_axes(ax_cb)
                cb = plt.colorbar(im, cax=ax_cb, ticks=n_bins_ranges, orientation='vertical', format='%.0f')
                ax.set_title("{0} (> {1:.0%} reals)".format(desc, pct))

                ax.set_xticks([])
                ax.set_yticks([])

            pdf.savefig()
            plt.close()

    # plot every 5 years the percentage of realizations in each area going dry or surpassing 1200 ft dtw
    vmin=0.0
    vmax = 1.0
    cm, norm, n_bins_ranges = plot_args(cmap="viridis_r", n_bin=5, vmin=vmin, vmax=vmax)

    with PdfPages(os.path.join(o_d, "{0}_DryDeepMap_byYear.pdf".format(forecast_name))) as pdf:
        for i, yr in obs.groupby("year"):
            if i % 5 == 0:
                print(i)
                temp0 = yr.loc[yr["pct_dry"]>0.0].copy()
                temp1 = yr.loc[yr["pct_low"]>0.0].copy()

                f, axs = plt.subplots(1,2,figsize=(11, 8.5), dpi=100)

                for temp, flg, desc, ax in zip([temp0, temp1], ["pct_dry", "pct_low"], ["dry", "DTW > 1200 ft"], axs.flat):
                    mshp.geometry.boundary.plot(ax=ax, facecolor="None", edgecolor="0.5", linewidth=0.5)
                    im = ax.scatter(temp.loc[:, "x"], temp.loc[:, "y"], c=temp.loc[:, flg], cmap=cm, norm=norm)

                    divider = make_axes_locatable(ax)
                    ax_cb = divider.new_horizontal(size="5%", pad=0.2, pack_start=False)
                    f.add_axes(ax_cb)
                    cb = plt.colorbar(im, cax=ax_cb, ticks=n_bins_ranges, orientation='vertical', format='%.2f')
                    ax.set_title("{0} percent of realizations {1}".format(i, desc))

                    ax.set_xticks([])
                    ax.set_yticks([])

                f.suptitle("{0}    {1} total realizations".format(forecast_name, int(nreal)))
                pdf.savefig()
                plt.close()


def plot_hds_contours(m_d="model_test_master", o_d=os.path.join("model_plots", "model_test_master"),
                      pst_name="hvhm_forecast.pst", a_d=None, forecast_name="model_test", map_years=[2022, 2032]):
    # add path to exported shapefiles if needed
    o_d1 = os.path.join(o_d, "shapefiles")
    if os.path.exists(o_d1):
        pass
    else:
        os.mkdir(o_d1)

    # load model object
    m = flopy.modflow.Modflow.load("hvhm_tr.nam",model_ws=m_d,check=False,forgive=False, load_only=["DIS", "BAS6"])    
    mg = m.modelgrid

    # import model grid for inset location plot
    model_shp = gp.read_file(os.path.join('hvhm_grid','hvhm_model_grid.shp'))
    modshp = model_shp.filter(['row', 'column', 'botm_1', 'top', 'ibound_1', 'geometry'])
    modshp.loc[:, "i"] = modshp.loc[:, "row"] - 1
    modshp.loc[:, "j"] = modshp.loc[:, "column"] - 1

    # import pst object
    pst = pyemu.Pst(os.path.join(m_d, pst_name))
    
    # import observation ensembles
    fname = "{0}.0.obs.jcb".format(pst_name.split(".")[0])
    oe_pt = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(m_d, fname))

    if a_d:
        oe_pa = pyemu.ObservationEnsemble.from_binary(pst=pst, filename=os.path.join(a_d, fname))

    # retrieve head map observations
    obs = pst.observation_data.loc[pst.observation_data['obgnme'].apply(lambda x: x.startswith("map_"))].copy()
    obs.loc[:,"k"] = obs.obsnme.apply(lambda x: int(x.split("_")[1]))
    obs.loc[:,"i"] = obs.obsnme.apply(lambda x: int(x.split("_")[2]))
    obs.loc[:,"j"] = obs.obsnme.apply(lambda x: int(x.split("_")[3]))
    obs.loc[:,"x"] = [mg.xcellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]
    obs.loc[:,"y"] = [mg.ycellcenters[i,j] for i,j in zip(obs.loc[:,"i"], obs.loc[:,"j"])]

    obs.loc[:, "mean"] = oe_pt.loc[:, obs.index].mean()
    obs.loc[:, "min"] = oe_pt.loc[:, obs.index].min()
    obs.loc[:, "max"] = oe_pt.loc[:, obs.index].max()

    for nm, grp in obs.groupby("obgnme"):
        obs_date = nm.split("_")[-1]
        modshp.loc[:, "temp"] = modshp.apply(lambda x: "map_00_{0:03d}_{1:03d}_{2}".format(x.i, x.j, obs_date), axis=1)
        for stat in ["mean", "min", "max"]:
            colname = "{0}_{1}_hds".format(nm, stat)
            modshp = modshp.join(grp.filter([stat]), how="left", on="temp")
            modshp.rename(columns={stat: colname}, inplace=True)
            # calc dtw and sat thick
            modshp.loc[:, "{0}_{1}_dtw".format(nm, stat)] = modshp.loc[:, "top"] - modshp.loc[:, colname]
            modshp.loc[:, "{0}_{1}_sat".format(nm, stat)] = modshp.loc[:, colname] - modshp.loc[:, "botm_1"]

    # calculate drawdowns in later map_year(s) from first map_year
    future_years = np.sort(obs.obgnme.unique())[1:]
    zero_year = np.sort(obs.obgnme.unique())[0]
    for yr in future_years:
        modshp.loc[:, "{0}_mean_ddn".format(yr)] = modshp.loc[:, "{0}_mean_hds".format(zero_year)] - modshp.loc[:, "{0}_mean_hds".format(yr)]

    # export contour shapefiles (CONVERTING TO FEET FROM METERS)
    shpcols = [x for x in modshp.columns if "mean" in x]
    
    # mask outputs where 2022 sat thickness close to zero
    mask_arr = np.zeros((m.nrow, m.ncol))
    mask_arr[modshp.loc[:, "i"], modshp.loc[:, "j"]] = modshp.loc[:, "map_20230101_mean_sat"]
    mask_arr = np.where(mask_arr > 5., 1, 0)
    
    for col in shpcols:
        arr = np.zeros((m.nrow, m.ncol))
        arr[modshp.loc[:, "i"], modshp.loc[:, "j"]] = modshp.loc[:, col]
        arr = np.where(mask_arr==1, arr * 3.28084, np.nan)
        fieldname = "{0}_ft".format(col.split("_")[-1])

        if "hds" in col:
            levels = np.linspace(2500., 4000., 31)
        elif "sat" in col:
            levels = np.linspace(0., 2000., 41)
        elif "dtw" in col:
            levels = np.linspace(200., 1200., 51)
        elif "ddn" in col:
            levels = np.linspace(0., 500., 51)

        flopy.export.utils.export_array_contours(modelgrid=mg, filename=os.path.join(o_d1, "{0}_{1}_ft.shp".format(forecast_name, col)),
                                                 a=arr, fieldname=fieldname, levels = levels, epsg=26912)


if __name__ == "__main__":
    pst_name = "hvhm_forecast.pst"

    # PROVIDE NAME OF WELL FILE CSV LOCATED IN WELL_FILE_INPUTS FOLDER    
    well_file = input("\nProvide filename of csv file containing forecasted pumping/infiltration rates.\nFile must be located in 'well_file_inputs' folder\n>")
    if not well_file.endswith(".csv"):
        well_file = well_file + ".csv"

    assert os.path.exists(os.path.join("well_file_inputs", well_file))

    # CONFIRM WHETHER WELL FILE INCLUDES NEW KINGMAN MUNICIPAL PUMPING STARTING 2019
    yn = input("\nDoes your provided csv file include new forecasted pumping rates for Kingman Municipal Wells?\nEnter (y/n)>")
    if yn.lower() == "y":
        replace_muni_pumping = True
    elif yn.lower() == "n":
        replace_muni_pumping = False
    else:
        print("Must enter y or n.")

    assert(replace_muni_pumping in [True, False])

    # SPECIFY FUTURE YEARS FOR WHICH TO CREATE CONTOUR SHPFILES AND COMPARE TO 2022 CONTOURS
    map_years = input("Provide list of years to plot head contours\nMust be separated by commas (Ex: 2022,2032,2042)\n>")
    try:
        map_years = [int(x) for x in map_years.split(",")]
        if map_years[0] != 2022:
            map_years.insert(0, 2022)
    except:
        print("cannot parse input. Must provide list like 2022,2032")
        map_years = [2022, 2032]
    
    forecast_name = well_file.replace(".csv", "")

    m_d = "{0}_master".format(forecast_name)
    print("forecast model files will be built in folder '{0}'".format(m_d))

    o_d0 = "model_plots"
    if os.path.exists(o_d0):
        pass
    else:
        os.mkdir(o_d0)
    
    o_d = os.path.join(o_d0, m_d)
    if os.path.exists(o_d):
        pass
    else:
        os.mkdir(o_d)

    print("\nForecast model outputs will be saved to folder '{0}'".format(o_d))

    # CHOOSE NUMBER OF MODEL REALIZATIONS TO INCLUDE IN ENSEMBLE FORECAST (between 2 - 40)
    num_reals = int(input("\nSelect number of model realizations to simulate.\nEnter a number between 2 - 40\n>"))
    if num_reals > 40:
        num_reals = 40
    elif num_reals < 2:
        num_reals = 2

    yn = input("\nDo you want to compare forecast results to a previously completed scenario?\nEnter (y/n)>")
    if yn.lower() == "y":
        a_d = input("Enter master directory folder name of comparison forecast.\nExample: forecast_20220422_master\n>")
        assert(os.path.exists(os.path.join(a_d, "hvhm_forecast.0.obs.jcb")))
    else:
        a_d = None

    build_forecast(well_file=well_file, forecast_name=forecast_name, map_years=map_years, replace_muni_pumping=replace_muni_pumping)
    run_forecast(well_file=well_file, num_reals=num_reals, forecast_name=forecast_name)

    if forecast_name != "hvhm_report_model":
        plot_wel_ts(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
        plot_hds_ts(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
        plot_hds_ts_new(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    
    plot_dtw_ts(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    plot_dtw_hist(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    plot_wel_inputs(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    plot_rch_inputs(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    plot_hds_contours(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name, map_years=map_years)
    plot_drydeep(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
    plot_zbd_obs(m_d=m_d, o_d=o_d, pst_name=pst_name, a_d=a_d, forecast_name=forecast_name)
