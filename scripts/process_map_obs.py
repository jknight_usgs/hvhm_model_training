import os
import multiprocessing as mp
import numpy as np
import pandas as pd
from flopy.utils.binaryfile import HeadFile as hf

def process_map_obs():
    hdobj = hf("hvhm_tr.hds")
    times = hdobj.get_times()
    spdf_map = pd.read_csv("spdf_map.csv")

    ib = np.loadtxt("ibound_layer_1.ref")
    locs = np.where(ib > 1)

    with open("map_obs.dat", "w") as ofp:
        ofp.write("obsnme obsval\n")
        for ii, r in spdf_map.iterrows():
            hds = hdobj.get_data(totim=times[r.SP])[0]
            for i,j in zip(locs[0], locs[1]):
                obsnme = "map_00_{0:03d}_{1:03d}_{2}".format(int(i), int(j), r.obs_time)
                obsval = hds[i,j]

                ofp.write("{0} {1}\n".format(obsnme, obsval))

if __name__ == '__main__':
    mp.freeze_support()
    process_map_obs()