import os
import multiprocessing as mp
import numpy as np
import pandas as pd
from flopy.utils.binaryfile import HeadFile as hf

def process_new_obs():    
    hdobj = hf("hvhm_tr.hds")

    df = pd.read_csv("new_obs_locs.csv")
    spdf = pd.read_csv("spdf.csv")

    with open("new_obs.dat", "w") as ofp:
        ofp.write("obsnme obsval\n")
        for ii, r in df.iterrows():
            obgnme = r.obgnme
            k = int(r.layer) - 1
            i = int(r.row) - 1
            j = int(r.column) - 1

            ts = hdobj.get_ts((k,i,j))[:,1]
            for sp in range(len(ts)):
                obsnme = "{0}_{1}".format(obgnme, spdf.loc[sp, "obs_time"])
                ofp.write("{0} {1}\n".format(obsnme, ts[sp]))

if __name__ == '__main__':
    mp.freeze_support()
    process_new_obs()