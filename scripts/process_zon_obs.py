import os
import multiprocessing as mp
import numpy as np
import pandas as pd
import flopy

def process_zon_obs():
    #zon = flopy.utils.ZoneBudget.read_zone_file(os.path.join("hvhm_tr.zbr"))
    zon = np.loadtxt(os.path.join("hvhm_tr.zbr"), skiprows=2, dtype=int)
    zon = np.where(zon==3, 2, zon)
    aliases = {1: "kingman", 2: "hualapai"}
    df = flopy.utils.ZoneBudget(os.path.join("hvhm_tr.cbc"), zon, aliases=aliases).get_dataframes()
    df *= (365.25 / 1233.48)
    df.reset_index(inplace=True)
    df.loc[:, "datetime"] = df.loc[:, "totim"].apply(lambda x: pd.to_datetime("1934-12-31") + np.timedelta64(int(x), "D"))
    df.drop(["totim"], axis=1, inplace=True)
    df = df.loc[df["name"].apply(lambda x: "WELLS" in x)].copy()
    df.rename(columns={"name": "grp"}, inplace=True)
    df.loc[:, "grp"] = df.apply(lambda x: "{0}_{1}".format(x.grp, x.datetime.strftime(format="%Y%m%d")), axis=1)
    df.drop("datetime", axis=1, inplace=True)
    df.index = df.pop("grp")
    df = df.stack().reset_index()
    df.loc[:, "obsnme"] = df.apply(lambda x: "{0}_{1}".format(x.level_1, x.grp), axis=1)
    df.rename(columns={0: "obsval"}, inplace=True)
    df = df.filter(["obsnme", "obsval"])
    df.to_csv("zon_obs.dat", sep=" ", index=False)

if __name__ == '__main__':
    mp.freeze_support()
    process_zon_obs()