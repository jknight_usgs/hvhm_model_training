import os
import multiprocessing as mp
import numpy as np
import pandas as pd
from flopy.utils.binaryfile import HeadFile as hf

def process_arr_obs():
    sdf = pd.read_csv("arr_obs_locs.csv", index_col=0)
    sdf.loc[:, "locs"] = sdf.apply(lambda x: (x.k, x.i, x.j), axis=1)
    names = sdf.loc[:, "obs_site"].values.tolist()
    names.insert(0, "totim")
    locs = sdf.loc[:, "locs"].values.tolist()

    hdobj = hf("hvhm_tr.hds")
    data = hdobj.get_ts(locs)

    df = pd.DataFrame(data=data, columns=names)
    df.loc[:, "datetime"] = df.loc[:, "totim"].apply(lambda x: pd.to_datetime('31-12-1934') + np.timedelta64(int(x), "D"))
    df.loc[:, "datetime"] = df.loc[:, "datetime"].apply(lambda x: x.strftime(format="%Y%m%d"))
    df.index = df.pop("datetime")
    df.drop("totim", axis=1, inplace=True)

    df = df.stack().reset_index()
    df.loc[:, "obsnme"] = df.apply(lambda x: "{0}_{1}".format(x.level_1, x.datetime), axis=1)
    df.rename(columns={0: "obsval"}, inplace=True)
    df = df.filter(["obsnme", "obsval"])

    df.to_csv("arr_obs.dat", sep=" ", index=False)

if __name__ == '__main__':
    mp.freeze_support()
    process_arr_obs()