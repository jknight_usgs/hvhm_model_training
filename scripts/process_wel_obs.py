import os
import multiprocessing as mp
import numpy as np
import pandas as pd
from flopy.utils import CellBudgetFile as cb

def process_wel_obs():    
    cbc = cb("hvhm_tr.cbc")

    df = pd.read_csv("wel_obs_locs.csv")
    spdf = pd.read_csv("spdf.csv")

    with open("wel_obs.dat", "w") as ofp:
        ofp.write("obsnme obsval\n")
        for ii, r in df.iterrows():
            obgnme = r.obgnme
            k = int(obgnme.split("_")[1])
            i = int(obgnme.split("_")[2])
            j = int(obgnme.split("_")[3])

            ts = cbc.get_ts((k, i, j), text='           WELLS')[:, 1]
            for ii,rr in spdf.iterrows():
                # if str(rr.st_year) in df.columns:
                #     obsnme = "{0}_{1}".format(obgnme, rr.obs_time)
                #     ofp.write("{0} {1}\n".format(obsnme, ts[rr.SP] * 365.25 / 1233.48))
                obsnme = "{0}_{1}".format(obgnme, rr.obs_time)
                ofp.write("{0} {1}\n".format(obsnme, ts[rr.SP] * 365.25 / 1233.48))

if __name__ == '__main__':
    mp.freeze_support()
    process_wel_obs()